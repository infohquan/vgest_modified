



setwd("D:/INRAE-montpllier/Code/IrmaraTool/")

threshold <-c(51, 60, 81)
station <- "PARIS_05"
station_name <- "Paris"
reservoirRuleSet <- c(1:6)
networkSet<-1
distributionType <-4
Qfile <- 2000
sMode <- "BATCH"
startDate <- "01/01/2000"
endDate <- "31/12/3999"

n <- 6
m <-3

s<-T
iError <-T
PaChone <- T

ExportRDs <- function(y,threshold, reservoirRuleSet) {
  widths <- c(12, 20, 14, 22, 14, 22, 14, 22, 14, 22, 10, 10, 10, 10, 8, 8, 8)
  headLine <- 18
  s2 <- readLines("Vgest/RESULTAT/PaChrono.txt", n = headLine)[headLine]
  headers = trimws(unlist(
    read.fwf(textConnection(s2), widths = widths, as.is = TRUE),
    use.names = FALSE
  ))
  df <- read.fwf(
    file = "Vgest/RESULTAT/PaChrono.txt",
    col.names = headers,
    widths = widths,
    skip = headLine,
    strip.white = TRUE
  )
  df[, grep(pattern = "date", names(df))] <-
    lapply(df[, grep(pattern = "date", names(df))], as.Date, format = "%d/%m/%Y")
  names(df) <-
    c(
      "dateAval",
      "QX",
      "dateM1",
      "Vobj1",
      "dateM2",
      "Vobj2",
      "dateM3",
      "Vobj3",
      "dateM4",
      "Vobj4",
      "QXd0",
      "QXd1",
      "QXd2",
      "Suc",
      "Ite",
      "Test"
    )
  df2 <- dplyr::select(df, dateAval, Vobj1, Vobj2, Vobj3, Vobj4)
  df2 <- dplyr::arrange(df2, dateAval)
  rdsFile <- paste0("Vgest/", station_name, "_", y ,"_",threshold,"_", reservoirRuleSet ,".rds")
  saveRDS(df2, file = rdsFile)
}

for (i in 1:n) {
  for (j in 1:m) {

    writeLines(
      paste("01/01", threshold[j]),
      file.path("Vgest", "OBJECTIF", sMode)
    )
    # Write CHOIX input
    s[i] <-
      list(c(station,
             reservoirRuleSet[i],
             networkSet,
        paste0(Qfile, ".tsv"),
        sMode,
        1,
        startDate,
        endDate ,
        1,
        distributionType))
    writeLines(s[[i]], file.path(paste0("Vgest", "/PARAMETR"), "CHOIX.TXT"))
    # Run VGEST
    workingDir <- getwd()
    setwd("Vgest")
    file.remove(list.files(path = "RESULTAT", full.names = TRUE))
    iError[i] <- system(command = "VGEST.exe")
    setwd(workingDir)

     ExportRDs(Qfile,threshold[j],reservoirRuleSet[i])
  }
}




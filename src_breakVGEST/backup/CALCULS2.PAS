Unit CALCULS2;  {JC Bader, nov 2016}

interface

 uses Utilit,DECLARA;

 procedure C2_LectureFreqNonDep;
 procedure C2_LecturePuissanceQXdef_DecideTri;
 procedure C2_LectureParametresStat;
 PROCEDURE C2_Tri(var Ztab:YTab2; ZN:integer);
 FUNCTION C2_Quantile(ZTab:YTab2; ZN:integer; Zfreq:double): double;
 PROCEDURE C2_ISOFREQ(ZDateDebutFichier, ZDateDebut, ZDateFin:longint; var ZFic : YTypFOD; ZChaine : string; ZNorme : double; ZFormat : integer) ;

IMPLEMENTATION

{========================================================================}
{--Lecture du param�tre YFreqNondep : fr�quence de non d�passement des --}
{--valeurs de d�bit QMN dans les cours d'eau aux prises, utilis�es dans--}
{--les calculs d'�quilibrage des dur�es potentielles de remplissage.   --}
{========================================================================}

procedure C2_LectureFreqNonDep;
 begin
  assign(YFicResult4,YRepP+YNomFicFNonDep);
  {$i-}
  reset(YFicResult4);
  if ioresult=0 then
   begin
    readln(YFicResult4,YFreqNonDep);
    if ioresult<>0 then
     YFreqNonDep:=0.5
    else
     YFreqNonDep:=B_min(B_max(YFreqNonDep,0.005),0.995);
    close(YFicResult4);
   end
  else
   YFreqNonDep:=0.5;
  {$i+}
 end; {fin de C2_LectureFreqNonDep}

{======================================================================}
{--Lecture du param�tre YPuiss utilis� pour le calcul de la moyenne  --}
{--des d�faillance de d�bit dues au manque de capacit� des r�servoirs--}
{--et test l'ordre de trier ces d�faillances                         --}
{======================================================================}

procedure C2_LecturePuissanceQXdef_DecideTri;
 begin
  assign(YFicResult4,YRepP+YNomFicPuiss);
  {$i-}
  reset(YFicResult4);
  if ioresult=0 then
   begin
    readln(YFicResult4,YPuiss);
    if ioresult<>0 then
     YPuiss:=2
    else
     YPuiss:=B_min(B_max(YPuiss,0.1),5);
    close(YFicResult4);
   end
  else
   YPuiss:=2;
  assign(YFicResult4,YRepP+YNomFicCalcTri);
  reset(YFicResult4);
  if ioresult=0 then
   begin
    YTestCalcTri:=true;
    close(YFicResult4);
   end
  else
   YTestCalcTri:=false;
  {$i+}
 end; {fin de C2_LecturePuissanceQXdef_DecideTri}

{==========================================================================}
{--Lecture des param�tres YA et YB utilis�s pour le calcul des fr�quences--}
{--de non d�passement : f = (rang-YA)(effectif+YB)                       --}
{==========================================================================}

procedure C2_LectureParametresStat;

 var
  XFic : text;

 begin
   YNbTpsRet:=0;
   YChoixFreq:=false;
   assign(XFic,YRepP+YNomFicTpsRet);
   {$i-}
   reset(XFic);
   if ioresult=0 then
    begin
     while (not(eof(XFic))) and not(YChoixFreq) do
      begin
       readln(XFic,YA,YB);
       if ioresult=0 then
        YChoixFreq:=true;
      end;
     while (not(eof(XFic))) and (YNbTpsret<YNbmaxTpsRet) do
      begin
       readln(XFic,YTps);
       if ioresult=0 then
        if YTps>=2 then
         begin
          YNbTpsRet:=YNbTpsRet+1;
          YTpsRet[YNbTpsRet]:=YTps;
         end;
      end;
     close(XFic);
    end;
   {$i+}
   if YChoixFreq then
    begin
     YA:=B_min(B_max(0,YA),0.5);
     YB:=B_min(B_max(0,YB),1);
    end
   else
    begin
     YA:=0.5;
     YB:=0;
    end;
   if YNbTpsRet=0 then
    begin
     YNbTpsRet:=YNbmaxTpsRet;
     for Yi:=1 to YNbTpsRet do
      YTpsRet[Yi]:=YTpsRetParDefo[Yi];
    end;
   C2_Tri(YTpsRet,YNbTpsRet);
   if YTpsRet[1]=2 then
    YjTpsRet:=2
   else
    YjTpsRet:=1;
 end; {fin de C2_LectureParametresStat}


{===========================================================================}
{--Tri en ordre croissant des ZN premi�res valeurs d'un tableau ZTab      --}
{===========================================================================}

PROCEDURE C2_Tri(var Ztab:YTab2; ZN:integer);
 var
  Xi : integer;
  Xj : integer;
  Xk : integer;
  XTab : YTab2;
  XTest : boolean;

 begin
  for Xi:=1 to ZN do
   begin
    Xj:=0;
    repeat
     Xj:=Xj+1
    until (Xj=Xi) or (Xtab[Xj]>ZTab[Xi]);
    for Xk:=Xi downto (Xj+1) do
     XTab[Xk]:=XTab[Xk-1];
    XTab[Xj]:=ZTab[Xi];
   end;
   for Xi:=1 to ZN do
    begin
     ZTab[Xi]:=XTab[Xi];
    end;
 end; {fin de C2_Tri}

{===========================================================================}
{--Calcul de la valeur non d�pass�e � la fr�quence Zfreq, � partir d'un   --}
{--tableau ZTab de N valeurs tri�es en ordre croissant                    --}
{===========================================================================}

FUNCTION C2_Quantile(ZTab:YTab2; ZN:integer; Zfreq:double): double;
 var
  Xi : integer;
  Xpos: double;
 begin
  Xi:=B_minin(ZN-1,B_Maxin(1,trunc((ZN+YB)*ZFreq+YA)));
  XPos:=Xi-YA;
  Xpos:=ZFreq-XPos/(ZN+YB);
  XPos:=XPos*(ZTab[Xi+1]-ZTab[Xi]);
  C2_Quantile:=ZTab[Xi]+XPos*(ZN+YB)
 end; {fin de C2_Quantile}

{===========================================================================}
{--A partir d'une chronique contenue dans un fichier ZFic et commen�ant � --}
{--la date ZDateDebutFichier, �dition dans le fichier YFicResult4 de 3    --}
{--tableaux contenant pour chaque jour de l'ann�e : valeurs tri�es par    --}
{--ann�e, valeurs croissantes et quantiles pour des fr�quences pr�d�finies--}
{===========================================================================}

PROCEDURE C2_ISOFREQ (ZDateDebutFichier, ZDateDebut, ZDateFin:longint;
                      var ZFic : YTypFOD; ZChaine : string;
                      ZNorme : double; ZFormat : integer) ;

 {-------------------------------------------}
 Procedure XEdite(ZZvaleur: double);
  begin
   case ZFormat of
    1 : write(YFicResult4,ZZValeur:15:2,' ');
    2 : write(YFicResult4,ZZValeur:15:13,' ');
    3 : write(YFicResult4,ZZValeur:15:8,' ');
    4 : write(YFicResult4,ZZValeur:15:10,' ');
    end;
  end;
 {-------------------------------------------}

 var
  XDate : string[5];
  XChai2 : string[2];
  XValeur : double;
  XMois : integer;
  XJour : integer;
 begin

  {--calcul des ann�es de d�but et de fin de s�rie des volumes � 24h--}
  YJJ_MM_AAAA[YRangLac]:=B_Date(ZDateDebut);
  val(copy(YJJ_MM_AAAA[YRangLac],7,4),YAnDebutV,YCode);
  YJJ_MM_AAAA[YRangLac]:=B_Date(ZDateFin);
  val(copy(YJJ_MM_AAAA[YRangLac],7,4),YAnFinV,YCode);

  {--ECRITURE D'EN-T�TE DE FICHIER TEXTE DES RESULTATS--}
  rewrite(YFicResult4);
  writeln(YFicResult4,'Rang du calcul : ',YCompteur,' ;  ',YResume);
  B_Datation(YFicResult4);
  writeln(YFicResult4);
  writeln(YFicResult4,ZChaine);
  writeln(YFicResult4);
  writeln(YFicResult4,'De haut en bas : resultats classes par jour de l''annee (quantieme 1 a 365)');
  write(YFicResult4,'De gauche a droite : resultats classes par ANNEE, par RANG n de ',
                      'valeurs croissantes et par FREQUENCE de non depassement (f=(n-YA)/(Effectif+YB) avec YA=');
  writeln(YFicResult4,YA,' et YB=',YB,')');
  writeln(YFicResult4);
  write(yFicResult4,'ANNEE--> ');
  for YAn:=YAnDebutV to YAnFinV do
   write(YFicresult4,'           ',YAn:4,' ');
  write(YFicResult4,'      RANG-->   ');
  for YAn:=YAnDebutV to YAnFinV do
   write(YFicresult4,'           ',(YAn-YAnDebutV+1):4,' ');
  write(YFicResult4,'    RETOUR-->   ','                ');
  for Yi:=YNbTpsRet downto 1 do
   write(YFicResult4,'         ',YTpsRet[Yi]:6:2,' ');
  for Yi:=YjTpsRet to YNbTpsRet do
   write(YFicResult4,'         ',YTpsRet[Yi]:6:2,' ');
  writeln(YFicResult4);
  write(YFicResult4,'                        ');
  for YAn:=YAnDebutV to YAnFinV do
   write(YFicResult4,'                                ');
  write(YFicResult4,'  FREQUENCE-->   ','        maximum ');
  for Yi:=YNbTpsRet downto 1 do
   begin
    YFreq:=1-1/YTpsRet[Yi];
    write(YficResult4,'        ',YFreq:7:5,' ');
   end;
  for Yi:=YjTpsRet to YNbTpsRet do
   begin
    YFreq:=1/YTpsRet[Yi];
    write(YficResult4,'        ',YFreq:7:5,' ');
   end;
  writeln(YFicResult4,'        minimum');

  reset(ZFic);

  {--BOUCLE SUR LES MOIS DE L'ANNEE--}
  for XMois:=1 to 12 do
   begin

    {--BOUCLE SUR LES JOURS DU MOIS (en ann�e non bissextile)--}
    for XJour:=1 to B_LongMois(2001,XMois) do
     begin

      {--calcul pour le jour courant, de la premi�re et de la derni�re--}
      {--ann�e pour lesquelles on a un volume calcul� � 24h           --}
      if B_TJour(YAnDebutV,XMois,XJour)>=ZDateDebut then
       YAnPremier:=YAnDebutV
      else
       YAnPremier:=YAnDebutV+1;
      if B_TJour(YAnFinV,XMois,XJour)<=ZDateFin then
       YAnDernier:=YAnFinV
      else
       YAnDernier:=YAnFinV-1;

      {--construction de chaine date en format jj/mm--}
      str(XJour,XChai2);
      if XJour<10 then
       XChai2:='0'+XChai2;
      str(XMois,XDate);
      if XMois<10 then
       XDate:='0'+XDate;
      XDate:=XChai2+'/'+XDate;


      {--ECRITURE DES VALEURS TRIEES PAR ANNEE--}
      write(YFicResult4,'   ',XDate,' ');
      for YAn:=YAnDebutV to YAnPremier-1 do
       write(YFicResult4,'                ');
      for YAn:=YAnPremier to YAnDernier do
       begin
       seek(ZFic,B_TJour(YAn,XMois,XJour)-ZDateDebutFichier);
        read(ZFic,YValBonneDate[YRanglac,YAn-YAnPremier+1]);
        YValBonneDate[YRanglac,YAn-YAnPremier+1]:=YValBonneDate[YRanglac,YAn-YAnPremier+1]/ZNorme;
        XEdite(YValBonneDate[YRangLac,YAn-YAnPremier+1]);
       end;
      for YAn:=YAnDernier+1 to YAnFinV do
       write(YFicResult4,'                ');

      {--ECRITURE DES VALEURS TRIEES PAR VALEURS CROISSANTES--}
      write(YFicResult4,'        ',XDate,'   ');
      C2_Tri(YValBonneDate[YRangLac],YAnDernier-YAnPremier+1);
      for YAn:= YAnPremier to YAnDernier do
      XEdite(YValBonneDate[YRangLac,YAn-YAnPremier+1]);
      for YAn:=YAnDebutV to YAnPremier-1 do
       write(YFicResult4,'                ');
      for YAn:=YAnDernier+1 to YAnFinV do
       write(YFicResult4,'                ');

      {--ECRITURE DES VALEURS TRIEES PAR FREQ. DECROISSANTE DE NON DEPASSMT.--}
      write(YFicResult4,'        ',XDate,'   ');
      XEdite(YValBonneDate[YRanglac,YAnDernier-YAnPremier+1]);

      for Yi:=YNbTpsRet downto YjTpsRet do
       begin
        YFreq:=1-1/YTpsRet[Yi];

        Yk:=YAnDernier-YAnPremier+2;
        repeat
         Yk:=Yk-1;
        until ((Yk-YA)/(YAnDernier-YanPremier+1+YB))<YFreq;

       if Yk<(YAnDernier-YAnPremier+1) then

         begin
          XValeur:= ((YFreq-Yk-YA)/(YAndernier-YAnPremier+1+YB))*(YAnDernier-YAnPremier+1+YB);
          XValeur:= YValBonneDate[YRanglac,Yk] + (YValbonneDate[YRanglac,Yk-1]-YValBonneDate[YRanglac,Yk])*XValeur;
          XEdite(XValeur);
         end
        else
         write(YFicResult4,'                ');
       end;

      for Yi:=1 to YNbTpsRet do
       begin
        YFreq:=1/YTpsRet[Yi];
        Yk:=0;
        repeat
         Yk:=Yk+1;
        until ((Yk-YA)/(YAnDernier-YanPremier+1+YB))>YFreq;

        if Yk>1 then
        begin
          XValeur:=(YFreq-(Yk-1-YA)/(YAndernier-YAnPremier+1+YB))*(YAnDernier-YAnPremier+1+YB);
          XValeur:=YValBonneDate[YRanglac,Yk-1]+(YValbonneDate[YRangLac,Yk]-YValBonneDate[YRangLac,Yk-1])*XValeur;
          XEdite(XValeur);
         end
        else
         write(YFicResult4,'                ');
       end;
      XEdite(YValBonneDate[YRanglac,1]);
      writeln(YFicResult4);

     end; {fin de boucle sur les jours}

   end; {fin de boucle sur les mois}

  close(YFicResult4);
  close(ZFic);

 end; {fin de C2_ISOFREQ}

{=============================================================================}

BEGIN
END.

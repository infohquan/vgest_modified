UNIT ECRAN;

interface

uses crt, Utilit, Interfas, DECLARA;

procedure E_Message1;
procedure E_Saisie2;
procedure E_Saisie6;
procedure E_Saisie7;
procedure E_Message7;
{--procedure E_Message9;                     --}
procedure E_Message10;
procedure E_Message11;
{--procedure E_Message12;                   --}

IMPLEMENTATION

 procedure E_Message1;
  begin
   A_Window1;
   clrscr;
   A_titre;
  end;

 procedure E_saisie2;
  begin
   a_quest;
   writeln;
   write('type du d�bit objectif : minimal (');
   A_Sulad;
   write(0);
   a_quest;
   write(') ou maximal (');
   a_propo;
   write(1);
   a_quest;
   write(') ? ');
   A_choixOption(YTypeObjectif,0,1,0);
   writeln;
   A_titre;
   write('synchronisation des d�bits et analyse des lacunes : en cours');
   A_Lecture_xy;
  end;

 procedure E_Saisie6;
  begin
   gotoxy(Ax-8,Ay);
   clreol;
   writeln('termin�');
   writeln;
   A_Titre;
   write('p�riode la plus longue de r�sultats utilisables : ');
   writeln('du ',B_Date(YMeilleurDebut),' au ',B_Date(YMeilleurFin));
   writeln;
   A_quest;
   write('dates de d�but et fin des calculs (jjmmaaaa) ? ');
   A_Lecture_xy;
   A_Saisidate (YJour[0],YMois[0],YAn,YChaine,B_Date(YMeilleurDebut),B_Date(YMeilleurFin-3*365));
   YDateDebut:=B_TJour(YAn,YMois[0],YJour[0]);
   gotoxy(Ax+10,Ay);
   A_Saisidate (YJour[0],YMois[0],YAn,YChaine,B_Date(YDateDebut+3*365),B_Date(YMeilleurFin));
   YDateFin:=B_TJour(YAn,YMois[0],YJour[0]);
  end;

 procedure E_Saisie7;
  begin
   A_quest;
   writeln;
   write('r�sultats de volumes �dit�s en valeurs absolues (');
   A_Sulad; write(1); A_quest;
   write(') ou relatives (');
   A_Propo; write('2'); A_quest; write(') ? ');
   A_ChoixOption(YAbsRel,1,2,1);
   A_quest;
   writeln;
   write('mode de r�partition des stockages : fig�e (');
   A_propo; write(1); A_quest;
   write(') ou variable (');
   A_sulad; write('2'); A_quest; write(') ? ');
   A_ChoixOption(YOptionRepart,1,2,2);
   if YOptionRepart=2 then
    begin
     A_titre;
     writeln;
     writeln('options disponibles pour la r�partition variable :');
     A_Propo; write(' 2'); A_titre;
     writeln(' : r�partition en fonction de V et Tpot (duree potentielle minimale de');
     writeln('     reconstitution du volume utilisable maximal)');
     A_propo; write(' 3'); A_titre;
     writeln(' : r�partition visant � �quilibrer V/Vtot entre r�servoirs');
     A_Sulad; write(' 4'); A_titre;
     writeln(' : r�partition visant � �quilibrer, entre les r�servoirs, la dur�e');
     writeln('     potentielle minimale Tpot1 de reconstitution du volume utilisable maximal');
     A_propo; write(' 5'); A_titre;
     writeln(' : r�partition visant � �quilibrer, entre les r�servoirs, la dur�e');
     writeln('     potentielle minimale Tpot2 d''�puisement complet du volume utilisable');
     A_Quest;
     write('option choisie ? ');
     A_ChoixOption(YOptionRepart,2,5,4);
     if YOptionRepart>3 then
      begin
       YCodeTpot:=(YOptionRepart=4);
       A_titre;
       writeln;
       write('choix des d�bits dans les cours d''eau, � utiliser pour calculer TPot');
       if YOptionRepart=4 then
        write('1')
       else
        write('2');
       writeln(' :');
       A_Sulad; write(' 1'); A_titre;
       writeln(' : valeurs moyennes interannuelles');
       A_propo; write(' 2'); A_titre;
       writeln(' : hydrogrammes annuels isofr�quence');
       A_Quest;
       write('option choisie ? ');
       A_ChoixOption(YOptionRepart,1,2,1);
       YOptionRepart:=YOptionRepart+3;
      end;
    end;
   A_titre;
   writeln;
  end;

 procedure E_Message7;
  begin
   A_texte;
   write('la p�riode, inf�rieure � 3 ans, est trop courte. ABANDON du calcul');
   A_Tapez;
  end;
 {--
 procedure E_Message9;
  begin
   gotoxy(Ax,Ay);
   writeln('termin�e');
   writeln;
   write('analyse statistique des r�sultats : ');
   A_lecture_xy;
   write('en cours');
  end;                                                --}

 procedure E_Message10;
  begin
   gotoxy(Ax,Ay);
   writeln('termin�e');
   writeln;
   writeln('FIN NORMALE DU PROGRAMME');
   if YSaisie then
    A_Tapez;
  end;

 procedure E_Message11;
  begin
   A_window1;
   clrscr;
   A_Titre;
   writeln;
   writeln;
   writeln('ECHEC DU PROGRAMME');
   A_tapez;
  end;
 {--
 procedure E_Message12;
  begin
   write('  �dition du bilan : ');
   A_Lecture_xy;
   write ('en cours');
  end;                         --}

BEGIN

END.
